const { resolve } = require("path");
const { readdir, readFile, writeFile } = require("fs").promises;

async function* getFiles(dir) {
  const dirents = await readdir(dir, { withFileTypes: true });
  for (const dirent of dirents) {
    const res = resolve(dir, dirent.name);
    if (dirent.isDirectory()) {
      yield* getFiles(res);
    } else {
      yield res;
    }
  }
}

(async () => {
  let icons = ``;
  for await (const f of getFiles(".")) {
    if (!f.includes("script")) {
      const svg = await readFile(f, "utf-8");
      if (!svg.includes("IHDR")) {
        // const title = svg.match(/(?<=<title>)(.+)(?=<\/title>)/gm)[0];
        icons = `${icons}${cell(svg)}`;
      }
    }
    await writeFile("./report.html", template(icons), "utf8");
  }
})();

const cell = (body) => `<span onclick="(e => {
    if(this.style.backgroundColor === 'orange'){
        this.style.backgroundColor = ''
    }else{
        this.style.backgroundColor = 'orange'
    }
})()">${body}</span>`;

const template = (body) => `
<html>
<head>
<style>
div{
    width:90%;
    display:flex;
    flex-wrap:wrap;
    flex-direction:row;
}
span{
    padding:10px;
    display:flex;
    flex-direction:column;
    justify-content: center;
}
</style>
</head>
<body>
<div>
${body}
</div>

</body>
</html>
`;
